﻿using System;

namespace snake
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Game game = new Game();

            game.Start();

            Console.ReadKey();
        }
    }
}
