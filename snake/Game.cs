﻿using System;
using System.Threading;

namespace snake
{
    public enum SCENES
    {
        MENU,
        LEVEL,
        GAME_OVER
    }

    public class Game
    {
        const int MS_PER_FRAME = 120;

        Scene scene;

        public Game() { }

        public void Start()
        {
            MoveToScene(SCENES.LEVEL);

            while (true)
            {
                DateTime start = DateTime.Now;

                scene.ProcessInput();
                scene.Update();
                scene.Render();

                Thread.Sleep((DateTime.Now - start).Milliseconds + MS_PER_FRAME);
            }
        }

        void MoveToScene(SCENES target)
        {
            Console.Clear();

            switch (target)
            {
                case SCENES.MENU:
                    break;
                case SCENES.LEVEL:
                    scene = new LevelScene(MoveToScene);
                    break;
                case SCENES.GAME_OVER:
                    scene = new GameOverScene(MoveToScene);
                    break;
            }
        }
    }
}
