﻿using System;
namespace snake
{
    public static class Renderer
    {
        static Renderer()
        {
            Console.CursorVisible = false;
        }

        public static void RenerScene(Scene scene)
		{
            for (int i = 0; i < scene.Width; i++)
			{
                for (int j = 0; j < scene.Height; j++)
				{
					RenderPixel(scene.canvas[i, j]);
				}
			}
		}

		static void RenderPixel(Pixel pix)
		{
			Console.ForegroundColor = pix.Color;
			Console.SetCursorPosition(pix.X, pix.Y);
			Console.Write(pix.Data);
		}
    }
}
