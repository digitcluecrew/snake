﻿using System;
namespace snake
{
	public class Pixel
	{
		int x;
		int y;
		public int X
		{
			get { return x; }
		}
		public int Y
		{
			get { return y; }
		}
		public ConsoleColor Color { get; set; }
        public object Data { get; set; }

        public Pixel(int x, int y)
		{
			this.x = x;
			this.y = y;

            Data = ' ';
		}

        public void Update(object data, ConsoleColor color = ConsoleColor.Gray)
		{
			Data = data;
			Color = color;
		}
	}
}
