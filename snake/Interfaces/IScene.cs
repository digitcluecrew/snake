﻿namespace snake
{
    interface IScene
    {
        void ProcessInput();
        void Update();
        void Render();
    }
}
