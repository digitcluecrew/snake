﻿using System;
namespace snake
{
    enum DIRECTION
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public class Snake
    {
        DIRECTION direction = DIRECTION.DOWN;

        Position[] body;

        public int Size
        {
            get { return body.Length; }
        }

        public Position Head {
            get { return this[0]; }
            private set { this[0] = value; }
        }

        public Position Tail
        {
            get { return this[Size - 1]; }
        }

        public Position NextPosition
        {
            get
            {
                switch (direction)
                {
                    case DIRECTION.UP:
                        return new Position(Head.X, Head.Y - 1);
                    case DIRECTION.DOWN:
                        return new Position(Head.X, Head.Y + 1);
                    case DIRECTION.LEFT:
                        return new Position(Head.X - 1, Head.Y);
                    case DIRECTION.RIGHT:
                        return new Position(Head.X + 1, Head.Y);
                    default:
                        throw new Exception("Snake direction is undefined");
                }
            }
        }

        public Position this[int index]
        {
            get { return body[index]; }
            private set { body[index] = value; }
        }

        public Snake()
        {
            direction = DIRECTION.RIGHT;

            body = new Position[] { new Position(6, 3), new Position(5, 3), new Position(4, 3) };
        }

        public void TurnUp()
        {
            if (direction == DIRECTION.DOWN)
            {
                return;
            }

            direction = DIRECTION.UP;
        }

        public void TurnDown()
        {
            if (direction == DIRECTION.UP)
            {
                return;
            }

            direction = DIRECTION.DOWN;
        }

        public void TurnLeft()
        {
            if (direction == DIRECTION.RIGHT)
            {
                return;
            }

            direction = DIRECTION.LEFT;
        }

        public void TurnRight()
        {
            if (direction == DIRECTION.LEFT)
            {
                return;
            }

            direction = DIRECTION.RIGHT;
        }

        public void Move()
        {
            Position newHead = Tail;

            switch (direction)
            {
                case DIRECTION.UP:
                    newHead.X = Head.X;
                    newHead.Y = Head.Y - 1;
                    break;
                case DIRECTION.DOWN:
                    newHead.X = Head.X;
                    newHead.Y = Head.Y + 1;
                    break;
                case DIRECTION.LEFT:
                    newHead.X = Head.X - 1;
                    newHead.Y = Head.Y;
                    break;
                case DIRECTION.RIGHT:
                    newHead.X = Head.X + 1;
                    newHead.Y = Head.Y;        
                    break;
            }

            for (int i = Size - 1; i > 0; i--)
            {
                body[i] = body[i - 1];
            }

            Head = newHead;
        }

        public void Grow()
        {
            Position[] newBody = new Position[Size + 1];

            newBody[0] = NextPosition;

            for (int i = 0; i < Size; i++)
            {
                newBody[i + 1] = body[i];
            }

            body = newBody;
        }
    }
}
