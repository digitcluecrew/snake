﻿namespace snake
{
    public class Scene : IScene
    {
        public uint Width { get; set; }
        public uint Height { get; set; }
        public MoveToSceneDelegate MoveToScene { get; set; }
        public Pixel[,] canvas;

        public delegate void MoveToSceneDelegate(SCENES target);

        public Scene(MoveToSceneDelegate moveToScene, uint width = 40, uint height = 20)
        {
            MoveToScene = moveToScene;
            Width = width;
            Height = height;

            canvas = new Pixel[Width, Height];

            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    canvas[i, j] = new Pixel(i, j);
                }
            }
        }

        public virtual void ProcessInput() { }

        public virtual void Update() { }

        public void Render()
        {
            Renderer.RenerScene(this);
        }

    }
}
