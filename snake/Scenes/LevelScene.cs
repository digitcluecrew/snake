﻿using System;
using System.Collections.Generic;

namespace snake
{
    enum ACTION
    {
        IDLE,
        TURN_UP,
        TURN_DOWN,
        TURN_LEFT,
        TURN_RIGHT
    }

    public class LevelScene : Scene, IScene
    {
        const char SEGMENT_EMPTY = ' ';
        const char SEGMENT_WALL = '@';
        const char SEGMENT_SNAKE = '*';
        const char SEGMENT_MEAL = '%';

        uint paddingTop = 2;
        uint paddingBottom = 2;
        uint paddingLeft = 2;
        uint paddingRight = 2;

        Snake snake;

        ACTION action;

        public LevelScene(MoveToSceneDelegate moveToScene, uint width = 40, uint height = 20)
            : base(moveToScene, width, height)
        {
            snake = new Snake();

            UpdateCanvasBorders();
            UpdateSnake();
            UpdateMeal();
        }

        public override void ProcessInput()
        {
            while (Console.KeyAvailable)
            {
                ConsoleKeyInfo key = Console.ReadKey(true);

                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        action = ACTION.TURN_UP;
                        break;
                    case ConsoleKey.LeftArrow:
                        action = ACTION.TURN_LEFT;
                        break;
                    case ConsoleKey.RightArrow:
                        action = ACTION.TURN_RIGHT;
                        break;
                    case ConsoleKey.DownArrow:
                        action = ACTION.TURN_DOWN;
                        break;
                }
            }
        }

        public override void Update()
        {
            ClearSnake();
            MoveSnake();
            UpdateSnake();

            action = ACTION.IDLE;
        }

        void ClearSnake()
        {
            canvas[snake.Tail.X, snake.Tail.Y].Update(SEGMENT_EMPTY);
            //for (int i = 0; i < snake.Size; i++)
            //{
            //    canvas[snake[i].X, snake[i].Y].Update(' ');
            //}
        }

        void MoveSnake()
        {
            switch (action)
            {
                case ACTION.TURN_UP:
                    snake.TurnUp();
                    break;
                case ACTION.TURN_DOWN:
                    snake.TurnDown();
                    break;
                case ACTION.TURN_LEFT:
                    snake.TurnLeft();
                    break;
                case ACTION.TURN_RIGHT:
                    snake.TurnRight();
                    break;
            }

            DetectCollision();

            snake.Move();
        }

        void UpdateSnake()
        {
            for (int i = 0; i < snake.Size; i++)
            {
                canvas[snake[i].X, snake[i].Y].Update(SEGMENT_SNAKE, ConsoleColor.Green);
            }
        }

        void UpdateMeal()
        {
            Random rnd = new Random();

            int mealX = rnd.Next((int)paddingLeft + 1, (int)(Width - paddingRight));
            int mealY = rnd.Next((int)paddingTop + 1, (int)(Height - paddingBottom));

            do
            {
                mealX = rnd.Next((int)paddingLeft + 1, (int)(Width - paddingRight));
                mealY = rnd.Next((int)paddingTop + 1, (int)(Height - paddingBottom));

            } while ((char)canvas[mealX, mealY].Data == SEGMENT_SNAKE);

            Console.Title = String.Format("x: {0}, y: {1}", mealX, mealY);
            canvas[mealX, mealY].Update(SEGMENT_MEAL, ConsoleColor.Magenta);
        }

        void DetectCollision()
        {
            Pixel nextStep = canvas[snake.NextPosition.X, snake.NextPosition.Y];

            if (nextStep.Data == null)
            {
                return;
            }

            char nextSegment = (char)nextStep.Data;

            if (nextSegment == SEGMENT_WALL || nextSegment == SEGMENT_SNAKE)
            {
                MoveToScene.Invoke(SCENES.GAME_OVER);
            }

            if (nextSegment == SEGMENT_MEAL)
            {
                snake.Grow();

                UpdateMeal();
            }
        }

        void UpdateCanvasBorders()
        {

            for (uint i = paddingLeft + 1; i < Width - paddingRight; i++)
            {
                canvas[i, paddingTop].Update(SEGMENT_WALL, ConsoleColor.Gray);
                canvas[i, Height - paddingBottom].Update(SEGMENT_WALL, ConsoleColor.Gray);
            }

            for (uint i = paddingTop; i < Height - paddingBottom + 1; i++)
            {
                canvas[paddingLeft, i].Update(SEGMENT_WALL, ConsoleColor.Gray);
                canvas[Width - paddingRight, i].Update(SEGMENT_WALL, ConsoleColor.Gray);
            }
        }
    }
}
