﻿using System;

namespace snake
{
    class GameOverScene : Scene, IScene
    {
        string msg = "Game over!";

        public GameOverScene(MoveToSceneDelegate moveToScene, uint width = 40, uint height = 20)
            : base(moveToScene, width, height)
        {
            Console.Title = msg;
            UpdateCanvasBorders();
        }

        public override void Update()
        {
            base.Update();
            
            canvas[Width / 2 - msg.Length / 2, Height / 2].Update(msg);
        }

        void UpdateCanvasBorders()
        {

            for (uint i = 0; i < Width; i++)
            {
                canvas[i, 0].Update('@', ConsoleColor.Gray);
                canvas[i, Height - 1].Update('@', ConsoleColor.Gray);
            }

            for (uint i = 0; i < Height; i++)
            {
                canvas[0, i].Update('@', ConsoleColor.Gray);
                canvas[Width - 1, i].Update('@', ConsoleColor.Gray);
            }
        }
    }
}
